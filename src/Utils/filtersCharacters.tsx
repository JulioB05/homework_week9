const FiltersComics = {
  type: 'comics',
  data: [
    {
      id: 1886,
      name: 'Official Handbook of the Marvel Universe (2004) #12 (SPIDER-MAN)',
    },
    {
      id: 1332,
      name: 'X-Men: Days of Future Past (Trade Paperback)',
    },
    {
      id: 1590,
      name: 'Official Handbook of the Marvel Universe (2004) #9 (THE WOMEN OF MARVEL)',
    },
    { id: 123, name: 'New X-Men (2001) #150' },
    { id: 654, name: 'New X-Men (2004) #1' },
    { id: 95, name: 'Ultimate Fantastic Four (2003) #1' },
    {
      id: 1056,
      name: 'Exiles Vol. II: A World Apart (Trade Paperback)',
    },
  ],
};

const FiltersStories = {
  type: 'stories',
  data: [
    { id: 657, name: '3 of 3 - House of Broken Dreams' },
    { id: 2002, name: 'Interior #2002' },
    { id: 3000, name: '2 of 6 - Feral' },
    { id: 3550, name: 'Ultimate Extinction (2006) #3' },
    { id: 3150, name: '3 of 3 - Titania' },
    { id: 3900, name: '1 of 1 - Zemo vs. Swordsman' },
  ],
};
export { FiltersComics, FiltersStories };
