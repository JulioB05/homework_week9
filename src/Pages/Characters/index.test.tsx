/* eslint-disable testing-library/no-debugging-utils */
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Characters from '../Characters';

test('of rendering of Characters page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Characters />
    </Provider>
  );
  const data = {
    offset: 0,
    limit: 0,
    total: 0,
    count: 0,
    results: [],
  };
});
