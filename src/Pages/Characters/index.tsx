import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import FetchDataCharacters from '../../Components/FetchData/FetchDataCharacters';
import Footer from '../../Components/Footer';
import Header from '../../Components/Header';
import Loader from '../../Components/Loader';
import SearchBox from '../../Components/SearchBox';
import useFetch from '../../Hooks/useFetch';
import useResourceURL from '../../Hooks/useResourceURL';
import { Data } from '../../Interfaces/apiCharacters';
import { FiltersComics, FiltersStories } from '../../Utils/filtersCharacters';
import './style.scss';

const Characters = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [currentPage, setCurrentPage] = useState(1);
  const { getResourceCharacterURL } = useResourceURL();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
    type: searchParams.get('type'),
    id: searchParams.get('id'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceCharacterURL(
    'characters',
    currentPage,
    params.name,
    params.id,
    params.type
  );

  const { data, loading } = useFetch(url);

  const dataCharacters: Data = data?.data;

  const sizeOfData = data?.data?.total;

  return (
    <div className="body-characters">
      <Header />
      <SearchBox
        setSearchParams={setSearchParams}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        filter={FiltersComics}
        filter2={FiltersStories}
      />
      <FetchDataCharacters
        params={params}
        apiStatus={loading}
        data={dataCharacters}
      />
      <Loader apiState={loading} />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
        params={params}
      />
    </div>
  );
};

export default Characters;
