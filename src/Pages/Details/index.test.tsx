import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Details from '../Details';

test('of rendering of Details page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Details />
    </Provider>
  );
});
