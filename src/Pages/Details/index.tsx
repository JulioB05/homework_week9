import { useParams } from 'react-router-dom';
import DetailsCard from '../../Components/DetailCard';
import Header from '../../Components/Header';
import useFetchDetails from '../../Hooks/useFetchDetails';
import useResourceURL from '../../Hooks/useResourceURL';
import './style.scss';

interface DetailsProps {}

const Details = (props: DetailsProps) => {
  const {} = props;
  const { getResourceDetailsURL } = useResourceURL();
  const { type, id } = useParams();
  const url = getResourceDetailsURL(type, id);

  const { dataDetailObject } = useFetchDetails(url, type);

  return (
    <div className="details-body">
      <Header></Header>
      <DetailsCard data={dataDetailObject} />
    </div>
  );
};

export default Details;
