import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Bookmarks from '../Bookmarks';

test('of rendering of Bookmarks page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Bookmarks />
    </Provider>
  );
});
