import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Card from '../../Components/Card';
import FilterBar from '../../Components/FilterBar';
import Header from '../../Components/Header';
import useFiltered from '../../Hooks/useFiltered';
import './style.scss';
interface bookmarkType {
  id: number;
  name: string;
  type: string;
  img: string;
}
interface stateData {
  bookmarks: { bookmarks: bookmarkType[] };
  hiddens: { hiddens: bookmarkType[] };
}

const Bookmarks = () => {
  const state = useSelector((state) => state);
  const typedState = state as stateData;
  const { bookmarks } = typedState;

  const { hiddensIdsActive, favoritesIds } = useFiltered('bookmarks');

  useEffect(() => {
    favoritesIds();
    hiddensIdsActive();
  }, []);

  return (
    <div className="bookmarks-page">
      <Header></Header>

      <FilterBar filterOff={true} bookmarks={true}></FilterBar>
      <div className="bookmarks">
        <h1 className="title-products">Bookmarks</h1>
        <div className="cards">
          {bookmarks.bookmarks?.length ? (
            bookmarks.bookmarks?.map((item) => {
              const img = item.img
                ? item.img
                : 'https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png';
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  title={item.name}
                  imgUrl={img}
                  body={''}
                  date={''}
                  category={item.type}
                  bookmarksBtn
                ></Card>
              );
            })
          ) : (
            <div className="title-no-items">
              <h1 className="no-items">There are no bookmarks yet</h1>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Bookmarks;
