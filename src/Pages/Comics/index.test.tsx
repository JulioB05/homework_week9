/* eslint-disable testing-library/no-debugging-utils */
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Comics from '../Comics';

test('of rendering of Comics page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Comics />
    </Provider>
  );
});
