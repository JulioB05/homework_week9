import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import FetchDataComics from '../../Components/FetchData/FetchDataComics';
import Header from '../../Components/Header';
import SearchBox from '../../Components/SearchBox';
import useFetch from '../../Hooks/useFetch';
import useResourceURL from '../../Hooks/useResourceURL';
import './style.scss';

import Loader from '../../Components/Loader';

import Footer from '../../Components/Footer';
import { Data } from '../../Interfaces/apiComics';
import { FiltersFormat, FiltersTitle } from '../../Utils/filtersComics';

const Comics = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [currentPage, setCurrentPage] = useState(1);
  const { getResourceComicsURL } = useResourceURL();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
    type: searchParams.get('type'),
    id: searchParams.get('id'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceComicsURL(
    'comics',
    currentPage,
    params.name,
    params.id,
    params.type
  );

  const { data, loading } = useFetch(url);

  const dataComics: Data = data.data;
  const sizeOfData = data?.data?.total;

  return (
    <div className="body-comics">
      <Header />
      <SearchBox
        setSearchParams={setSearchParams}
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        filter={FiltersFormat}
        filter2={FiltersTitle}
      />
      <FetchDataComics params={params} apiStatus={loading} data={dataComics} />

      <Loader apiState={loading} />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
        params={params}
      />
    </div>
  );
};

export default Comics;
