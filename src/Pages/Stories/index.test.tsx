import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Stories from '../Stories';

test('of rendering of Stories page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Stories />
    </Provider>
  );
});
