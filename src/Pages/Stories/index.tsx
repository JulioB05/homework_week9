import { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import FetchDataStories from '../../Components/FetchData/FetchDataStories';
import Header from '../../Components/Header';
import useFetch from '../../Hooks/useFetch';
import useResourceURL from '../../Hooks/useResourceURL';
import './style.scss';

import FilterBar from '../../Components/FilterBar';
import Footer from '../../Components/Footer';
import { Data } from '../../Interfaces/apiStories';
import FilterCharacter from '../../Utils/filtersStories';

const Stories = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [currentPage, setCurrentPage] = useState(1);
  const { getResourceStoriesURL } = useResourceURL();

  const params = {
    page: searchParams.get('page'),
    name: searchParams.get('name'),
    type: searchParams.get('type'),
    id: searchParams.get('id'),
  };

  useEffect(() => {
    if (typeof params.page === 'string') {
      setCurrentPage(parseInt(params.page));
    } else if (params.page === null) {
      setCurrentPage(1);
    }
  }, [params.page]);

  const url = getResourceStoriesURL(
    'stories',
    currentPage,
    params.id,
    params.type
  );

  const { data, loading } = useFetch(url);

  const dataStories: Data = data.data;
  const sizeOfData = data?.data?.total;

  return (
    <div className="body-stories">
      <Header />
      <FilterBar setSearchParams={setSearchParams} filter={FilterCharacter} />
      <FetchDataStories
        params={params}
        apiStatus={loading}
        data={dataStories}
      />
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
        params={params}
      />
    </div>
  );
};

export default Stories;
