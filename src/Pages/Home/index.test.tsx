import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Home from '../Home';

test('of rendering of Home page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Home />
    </Provider>
  );
});
