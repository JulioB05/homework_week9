import Header from '../../Components/Header';
import SlideShow from '../../Components/SlideShow';
import hulk from '../../Images/characters/hulk.jpg';
import spiderman from '../../Images/characters/spiderman.jpg';
import thor from '../../Images/characters/thor.jpg';
import wolverine from '../../Images/characters/wolverine.jpg';
import image1 from '../../Images/Slider/image1.jpg';
import image2 from '../../Images/Slider/image2.jpg';
import image3 from '../../Images/Slider/image3.jpg';
import image4 from '../../Images/Slider/image4.jpg';
import image5 from '../../Images/Slider/image5.jpg';
import './style.scss';

import stan from '../../Images/stan.jpg';

const Home = () => {
  const images = [image1, image2, image3, image4, image5];
  return (
    <div className="home">
      <Header />;
      <SlideShow images={images} autoPlay={true} />
      <div className="section-stan">
        <img src={stan} alt="" className="stan-img" />
        <p className="text">
          "With great power comes great responsibility" (Uncle Ben's phrase to
          Peter Parker was inspired by President Franklin D. Roosevelt's last
          public address in 1945, two days before he died).
        </p>
      </div>
      <section className="someRandom">
        <h2>Some characters</h2>
        <div className="container2">
          <article>
            <div className="square">
              <img src={wolverine} alt="" className="square-img" />
            </div>

            <p className="text-name">Wolverine</p>
          </article>
          <article>
            <div className="square">
              <img src={spiderman} alt="" className="square-img" />
            </div>

            <p className="text-name">Spiderman</p>
          </article>
          <article>
            <div className="square">
              <img src={hulk} alt="" className="square-img" />
            </div>

            <p className="text-name">Hulk</p>
          </article>
          <article>
            <div className="square">
              <img src={thor} alt="" className="square-img" />
            </div>

            <p className="text-name">Thor</p>
          </article>
        </div>
      </section>
      <div className="footer">
        <h1>
          MARVEL <span>STUDIOS</span>
        </h1>
      </div>
    </div>
  );
};

export default Home;
