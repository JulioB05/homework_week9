import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Card from '../../Components/Card';
import FilterBar from '../../Components/FilterBar';
import Header from '../../Components/Header';
import useFiltered from '../../Hooks/useFiltered';
import './style.scss';

interface hiddenType {
  id: number;
  name: string;
  type: string;
  img: string;
}
interface stateData {
  bookmarks: { bookmarks: hiddenType[] };
  hiddens: { hiddens: hiddenType[] };
}
const Hiddens = () => {
  const state = useSelector((state) => state);
  const typedState = state as stateData;
  const { hiddens } = typedState;

  const { hiddensIdsActive, favoritesIds } = useFiltered('hiddens');
  useEffect(() => {
    favoritesIds();
    hiddensIdsActive();
  }, []);

  return (
    <div className="hiddens-page">
      <Header></Header>
      <FilterBar filterOff={true} hiddens={true}></FilterBar>
      <div className="hiddens">
        <h1 className="title-products">Hiddens</h1>
        <div className="cards">
          {hiddens.hiddens?.length ? (
            hiddens.hiddens?.map((item) => {
              const img = item.img
                ? item.img
                : 'https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png';
              return (
                <Card
                  key={item.id}
                  id={item.id}
                  title={item.name}
                  imgUrl={img}
                  body={''}
                  date={''}
                  category={item.type}
                  hiddensBtn
                ></Card>
              );
            })
          ) : (
            <div className="title-no-items">
              <h1 className="no-items">There are no items yet</h1>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Hiddens;
