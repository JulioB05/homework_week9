import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Hiddens from '../Hiddens';

test('of rendering of Hiddens page', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Hiddens />
    </Provider>
  );
});
