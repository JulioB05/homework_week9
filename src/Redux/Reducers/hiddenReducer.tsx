import { actionHiddenTypes } from '../Actions/hiddenActions';
import { addToHidden, removeAllHiddens, removeOneHidden } from '../Types';

interface hideObj {
  id: number;
  name: string;
  type: string;
  img: string;
}
export interface hiddensType {
  hiddens: hideObj[];
}

export const initialStateHiddens: hiddensType = {
  hiddens: [],
};

const hiddensReducer = (
  state = initialStateHiddens,
  action: actionHiddenTypes
) => {
  switch (action.type) {
    case addToHidden: {
      return {
        ...state,
        hiddens: [...state.hiddens, action.payload],
      };
    }

    case removeOneHidden: {
      let newData = state.hiddens.filter((el) => el.id !== action.payload);
      return {
        ...state,
        hiddens: newData,
      };
    }

    case removeAllHiddens: {
      return {
        ...state,
        hiddens: [],
      };
    }
    default:
      return state;
  }
};

export default hiddensReducer;
