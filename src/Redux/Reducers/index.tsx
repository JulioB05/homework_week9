import { combineReducers } from 'redux';

import bookmarksReducer from './bookmarkReducer';
import hiddensReducer from './hiddenReducer';

const reducer = combineReducers({
  bookmarks: bookmarksReducer,
  hiddens: hiddensReducer,
});

export default reducer;
