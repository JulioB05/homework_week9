import '@testing-library/jest-dom/extend-expect';
import bookmarksReducer from '../Reducers/bookmarkReducer';

describe('render content', () => {
  const initialState = {
    bookmarks: [
      { id: 1, name: 'a', type: 'a', img: 'a' },
      { id: 2, name: 'b', type: 'b', img: 'b' },
    ],
  };

  test('testing of addToBookmark', () => {
    const dataMock = { id: 0, name: '', type: '', img: '' };
    const actionToDispatch = {
      type: 'addToBookmark' as const,
      payload: dataMock,
    };

    const addBookmark = bookmarksReducer(initialState, actionToDispatch);

    expect(addBookmark).toEqual({
      bookmarks: [
        { id: 1, name: 'a', type: 'a', img: 'a' },
        { id: 2, name: 'b', type: 'b', img: 'b' },
        { id: 0, name: '', type: '', img: '' },
      ],
    });
  });

  test('testing of removeOneBookmark', () => {
    const idMock = 1;
    const actionToDispatch = {
      type: 'removeOneBookmark' as const,
      payload: idMock,
    };
    const deleteOneBookmark = bookmarksReducer(initialState, actionToDispatch);
    expect(deleteOneBookmark).toEqual({
      bookmarks: [{ id: 2, name: 'b', type: 'b', img: 'b' }],
    });
  });

  test('testing of removeAllBookmarks', () => {
    const actionToDispatch = {
      type: 'removeAllBookmarks' as const,
    };
    const deleteAllBookmark = bookmarksReducer(initialState, actionToDispatch);

    expect(deleteAllBookmark).toEqual({
      bookmarks: [],
    });
  });
});
