import '@testing-library/jest-dom/extend-expect';
import hiddenReducer from '../Reducers/hiddenReducer';

describe('render content', () => {
  const initialState = {
    hiddens: [
      { id: 1, name: 'a', type: 'a', img: 'a' },
      { id: 2, name: 'b', type: 'b', img: 'b' },
    ],
  };

  test('testing of addToHidden', () => {
    const dataMock = { id: 0, name: '', type: '', img: '' };
    const actionToDispatch = {
      type: 'addToHidden' as const,
      payload: dataMock,
    };

    const addHidden = hiddenReducer(initialState, actionToDispatch);

    expect(addHidden).toEqual({
      hiddens: [
        { id: 1, name: 'a', type: 'a', img: 'a' },
        { id: 2, name: 'b', type: 'b', img: 'b' },
        { id: 0, name: '', type: '', img: '' },
      ],
    });
  });

  test('testing of removeOneHidden', () => {
    const idMock = 1;
    const actionToDispatch = {
      type: 'removeOneHidden' as const,
      payload: idMock,
    };
    const deleteOneHidden = hiddenReducer(initialState, actionToDispatch);
    expect(deleteOneHidden).toEqual({
      hiddens: [{ id: 2, name: 'b', type: 'b', img: 'b' }],
    });
  });

  test('testing of removeAllHiddens', () => {
    const actionToDispatch = {
      type: 'removeAllHiddens' as const,
    };
    const deleteAllHiddens = hiddenReducer(initialState, actionToDispatch);

    expect(deleteAllHiddens).toEqual({
      hiddens: [],
    });
  });
});
