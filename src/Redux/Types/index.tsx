//Bookmarks
export const addToBookmark = 'addToBookmark';
export const removeOneBookmark = 'removeOneBookmark';
export const removeAllBookmarks = 'removeAllBookmarks';

//Hiddens
export const addToHidden = 'addToHidden';
export const removeOneHidden = 'removeOneHidden';
export const removeAllHiddens = 'removeAllHiddens';
