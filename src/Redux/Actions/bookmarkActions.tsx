import { addToBookmark, removeAllBookmarks, removeOneBookmark } from '../Types';

export type actionBookmarkTypes =
  | {
      type: 'addToBookmark';
      payload: { id: number; name: string; type: string; img: string };
    }
  | { type: 'removeAllBookmarks' }
  | { type: 'removeOneBookmark'; payload: number };

export const addBookmark = (data: {
  id: number;
  name: string;
  type: string;
  img: string;
}) => ({
  type: addToBookmark,
  payload: data,
});

export const deleteOneBookmark = (id: number) => ({
  type: removeOneBookmark,
  payload: id,
});
export const deleteAllBookmark = () => ({
  type: removeAllBookmarks,
});
