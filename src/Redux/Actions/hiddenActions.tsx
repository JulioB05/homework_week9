import { addToHidden, removeAllHiddens, removeOneHidden } from '../Types';

export type actionHiddenTypes =
  | {
      type: 'addToHidden';
      payload: { id: number; name: string; type: string; img: string };
    }
  | { type: 'removeAllHiddens' }
  | { type: 'removeOneHidden'; payload: number };

export const addHidden = (data: {
  id: number;
  name: string;
  type: string;
  img: string;
}) => ({
  type: addToHidden,
  payload: data,
});

export const deleteOneHidden = (id: number) => ({
  type: removeOneHidden,
  payload: id,
});
export const deleteAllHidden = () => ({
  type: removeAllHiddens,
});
