import { legacy_createStore as createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducer from '../Reducers';

const persistConfiguration = {
  key: 'root',
  storage,
};

const persistReducerApp = persistReducer(persistConfiguration, reducer);
const store = createStore(persistReducerApp);

const persistor = persistStore(store);

export { store, persistor };
