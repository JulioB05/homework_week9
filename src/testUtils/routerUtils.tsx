import { render } from '@testing-library/react';
import { FunctionComponent, ReactElement, ReactNode } from 'react';
import { BrowserRouter, useLocation } from 'react-router-dom';

type WrapperPropsT = {
  children: ReactNode;
};

const LocationDisplay = () => {
  const location = useLocation();
  return <div data-testid="location-display">{location.pathname}</div>;
};

const Wrapper = ({ children }: WrapperPropsT) => (
  <BrowserRouter>
    {children}
    <LocationDisplay />
  </BrowserRouter>
);

const renderWithWrapper = (ui: ReactElement, route = '/') => {
  window.history.pushState({}, 'Test Page', route);
  return render(ui, { wrapper: Wrapper as FunctionComponent });
};

export default renderWithWrapper;
