import { useSelector } from 'react-redux';
interface hiddenType {
  id: number;
  name: string;
  type: string;
  img: string;
}
interface stateData {
  bookmarks: { bookmarks: hiddenType[] };
  hiddens: { hiddens: hiddenType[] };
}

const useFiltered = (type: string) => {
  const state = useSelector((state) => state);
  const typedState = state as stateData;
  const { bookmarks, hiddens } = typedState;

  const numberHiddens = hiddens.hiddens.length;
  const numberBookmarks = bookmarks.bookmarks.length;

  const hiddenIds = () => {
    let ids: number[] = [];
    hiddens.hiddens.forEach((item) => {
      if (item.type === type) {
        ids.push(item.id);
      }
    });
    return ids;
  };

  const favoritesIds = () => {
    bookmarks.bookmarks.forEach((item) => {
      if (item.type === type) {
        document
          .getElementById(`${item.type}/${item.id}/bookmark`)
          ?.classList.add('heart-active');
      }
    });
    if (type === 'hiddens') {
      bookmarks.bookmarks.forEach((item) => {
        document
          .getElementById(`${item.type}/${item.id}/bookmark`)
          ?.classList.add('heart-active');
      });
    }
  };

  const hiddensIdsActive = () => {
    if (type === 'bookmarks') {
      hiddens.hiddens.forEach((item) => {
        document
          .getElementById(`${item.type}/${item.id}/hide`)
          ?.classList.add('hide-active');
      });
    }
  };

  return {
    hiddenIds,
    favoritesIds,
    hiddensIdsActive,
    numberHiddens,
    numberBookmarks,
  };
};
export default useFiltered;
