import axios from 'axios';
import { useEffect, useState } from 'react';
import { Comics } from '../Interfaces/apiCharacters';
import { Characters, Stories } from '../Interfaces/apiComics';

interface dataDetailsTypes {
  id: number;
  title: string;
  body: string;
  image: string;
  comics?: Comics | null;
  characters?: Characters | null;
  stories?: Stories | null;
}

const useFetchDetails = (url: string, typeFromPage: string | undefined) => {
  const [data, setData] = useState(Object);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const dataDetails: dataDetailsTypes = {
    id: 0,
    title: 'title',
    body: 'body',
    image: 'imageDet',
  };
  const [dataDetailObject, setDataDetailsObject] = useState(dataDetails);

  useEffect(() => {
    setLoading(true);
    axios
      .get(url)
      .then((response) => {
        const { results } = response.data.data;
        const {
          id,
          description,
          title,
          comics,
          characters,
          stories,
          thumbnail,
          name,
        } = results[0];

        setData(response.data);

        const dataObject = {
          id: id,
          title: title,
          body: description,
          image: '',
          comics,
          characters,
          stories,
        };

        switch (typeFromPage) {
          case 'stories':
            dataObject.image = `https:www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png`;
            dataObject.comics = comics;
            dataObject.characters = characters;
            setDataDetailsObject(dataObject);
            break;
          case 'comics':
            dataObject.characters = characters;
            dataObject.stories = stories;
            dataObject.image = `${thumbnail.path}.${thumbnail.extension}`;
            setDataDetailsObject(dataObject);
            break;
          case 'characters':
            dataObject.title = name;
            dataObject.comics = comics;
            dataObject.stories = stories;
            dataObject.image = `${thumbnail.path}.${thumbnail.extension}`;
            setDataDetailsObject(dataObject);
            break;

          default:
        }
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url]);

  return { dataDetailObject, data, loading, error };
};

export default useFetchDetails;
