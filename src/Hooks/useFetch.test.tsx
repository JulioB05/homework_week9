/* eslint-disable testing-library/no-unnecessary-act */
/* eslint-disable @typescript-eslint/no-unused-vars */
import '@testing-library/jest-dom/extend-expect';
import { renderHook } from '@testing-library/react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { act } from 'react-dom/test-utils';
import useFetch from './useFetch';

interface initialValueType {
  data: any;
  loading: boolean;
  error: null;
}

test('useFetch performs GET request', async () => {
  const mock = new MockAdapter(axios);
  const mockData = 'response';
  const url = 'http://mock';

  mock.onGet(url).reply(200, mockData);
  const { result } = renderHook(() => useFetch(url));
  act(() => {
    expect(result.current.data).toEqual({});
    expect(result.current.loading).toBeTruthy();
  });
});
