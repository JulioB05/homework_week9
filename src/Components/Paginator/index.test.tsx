import '@testing-library/jest-dom/extend-expect';
import { act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Paginator from '../Paginator';

const setCurrentPageMocked = jest.fn;
const setSearchParamsMocked = jest.fn;
const setClickedMocked = jest.fn;

describe('rendering Paginator component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Paginator
        currentPage={1}
        setCurrentPage={setCurrentPageMocked}
        setSearchParams={setSearchParamsMocked}
        sizeOfData={20}
      />
    </Provider>
  );

  test('Call the nextFunction when user click the button next', async () => {
    const user = userEvent.setup();
    const button = screen.getByTestId('button-next');

    await act(async () => {
      await user.click(button);
    });

    expect(setSearchParamsMocked).toHaveBeenCalled();
    expect(setClickedMocked).toHaveBeenCalled();
  });
});
