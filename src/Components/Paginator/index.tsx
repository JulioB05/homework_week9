import { useEffect, useMemo, useState } from 'react';
import './style.scss';

interface PaginationProps {
  currentPage: number;
  setCurrentPage: Function;
  setSearchParams: Function;
  sizeOfData: number;
  params?: { page: string | null; name: string | null };
}

const Paginator = (props: PaginationProps) => {
  const { currentPage, setCurrentPage, setSearchParams, sizeOfData, params } =
    props;
  const [clicked, setClicked] = useState(true);
  const [slicePages, setSlicePages] = useState(1);

  const maxNumberPage = useMemo(() => {
    return Math.ceil(sizeOfData / 20);
  }, [sizeOfData]);

  const arrayPages = [];
  for (let i = 0; i < maxNumberPage; i++) {
    const a = {
      id: i + 1,
      numberPage: i + 1,
    };

    arrayPages.push(a);
  }

  const indexOfLastPage = slicePages * 8;
  const indexOfFirstPage = indexOfLastPage - 8;
  const currentButtonsPages = arrayPages.slice(
    indexOfFirstPage,
    indexOfLastPage
  );

  useEffect(() => {
    if (currentPage > indexOfLastPage) {
      setSlicePages(slicePages + 1);
    } else if (currentPage < indexOfFirstPage + 1) {
      setSlicePages(slicePages - 1);
    }
  }, [currentPage]);

  const HandleChange = (page: number) => {
    const presentParams = getNewParams(page, '');
    setCurrentPage(page);
    setSearchParams(presentParams);
  };

  const nextPage = () => {
    const nextPage = currentPage + 1;
    const nextParams = getNewParams(nextPage, params!.name);
    setCurrentPage(nextPage);
    setClicked(true);
    setSearchParams(nextParams);
  };

  const prevPage = () => {
    const prevPage = currentPage - 1;
    const previousParams = getNewParams(prevPage, params!.name);
    setCurrentPage(prevPage);
    setClicked(true);
    setSearchParams(previousParams);
  };

  const getNewParams = (pageData: number, nameData: string | null) => {
    if (typeof nameData === 'string' && nameData !== '') {
      const newParams = { name: nameData, page: pageData };
      return newParams;
    }
    const newParams = { page: pageData };
    return newParams;
  };

  return (
    <div>
      <div className="pagination">
        <button
          className="BtnNextPrev"
          onClick={prevPage}
          disabled={currentPage === 1}
        >
          PREVIOUS
        </button>

        {arrayPages.length
          ? currentButtonsPages?.map((page) => {
              const { id, numberPage } = page;
              return (
                <button
                  key={id}
                  onClick={() => HandleChange(numberPage)}
                  className={`pageNumberList ${
                    currentPage === id && clicked ? ' active' : ''
                  }`}
                >
                  {numberPage}
                </button>
              );
            })
          : null}

        <button
          className="BtnNextPrev"
          onClick={nextPage}
          disabled={currentPage === maxNumberPage}
          data-testid={'button-next'}
        >
          NEXT
        </button>
      </div>
    </div>
  );
};

export default Paginator;
