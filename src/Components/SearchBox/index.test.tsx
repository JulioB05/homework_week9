import '@testing-library/jest-dom/extend-expect';
import * as _ from 'lodash';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import SearchBox from '../SearchBox';

test('of rendering of SearchBox component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <SearchBox
        setSearchParams={Function}
        currentPage={0}
        setCurrentPage={Function}
      />
    </Provider>
  );
});

jest.useFakeTimers();

describe('debounce', () => {
  let func: jest.Mock;
  let debouncedFunc: Function;

  beforeEach(() => {
    func = jest.fn();
    debouncedFunc = _.debounce(func, 1000);
  });

  test('execute just once', () => {
    for (let i = 0; i < 100; i++) {
      debouncedFunc();
    }

    // Fast-forward time
    jest.runAllTimers();

    expect(func).toBeCalledTimes(1);
  });
});
