import { useEffect, useState } from 'react';
import './style.scss';

interface SlideShowProps {
  images: string[];
  autoPlay?: boolean;
  showButtons?: boolean;
}

const SlideShow = (props: SlideShowProps) => {
  const { images, autoPlay, showButtons } = props;
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [selectedImage, setSelectedImage] = useState(images[0]);
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (autoPlay || !showButtons) {
      const interval = setInterval(() => {
        selectNewImage(selectedIndex, props.images);
      }, 5000);
      return () => clearInterval(interval);
    }
  });

  const selectNewImage = (index: number, images: string[], next = true) => {
    setLoaded(false);
    setTimeout(() => {
      const condition = next
        ? selectedIndex < images.length - 1
        : selectedIndex > 0;
      const nextIndex = next
        ? condition
          ? selectedIndex + 1
          : 0
        : condition
        ? selectedIndex - 1
        : images.length - 1;
      setSelectedImage(images[nextIndex]);
      setSelectedIndex(nextIndex);
    }, 500);
  };

  const previous = () => {
    selectNewImage(selectedIndex, images, false);
  };

  const next = () => {
    selectNewImage(selectedIndex, images);
  };

  return (
    <div className="carousel">
      <div className={`carouselImg  `}>
        <img
          src={selectedImage}
          alt="ImageSlider"
          onLoad={() => setLoaded(true)}
          className={loaded ? 'loaded' : ''}
        />
      </div>
      <div className="carouselButtonContainer">
        {props.showButtons ? (
          <>
            <button className="carouselButtonPrev" onClick={previous}>
              {'<'}
            </button>
            <button className="carouselButtonNext" onClick={next}>
              {'>'}
            </button>
          </>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default SlideShow;
