import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import SlideShow from '../SlideShow';

test('of rendering of SlideShow component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <SlideShow images={[]} />
    </Provider>
  );
});
