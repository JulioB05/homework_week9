import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Loader from '../Loader';

test('of rendering of Loader component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Loader apiState={false} />
    </Provider>
  );
});
