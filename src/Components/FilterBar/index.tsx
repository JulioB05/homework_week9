import { useState } from 'react';
import Filter from '../Filter';
import Modal from '../Modal';
import './style.scss';
interface FilterBarProps {
  setSearchParams?: Function;
  filter?: { type: string; data: { id: number; name: string }[] };
  filterOff?: boolean;
  bookmarks?: boolean;
  hiddens?: boolean;
}
const FilterBar = (props: FilterBarProps) => {
  const { setSearchParams, filter, filterOff, bookmarks, hiddens } = props;

  const [modalState, setModalState] = useState(false);
  const [typeItem, setTypeItem] = useState('');

  const deleteBookmarks = () => {
    setTypeItem('all-bookmarks');
    setModalState(true);
  };

  const deleteHiddens = () => {
    setTypeItem('all-hiddens');
    setModalState(true);
  };

  return (
    <div>
      <div className="filter-bar-form">
        <div className="form-group">
          <div className="filters">
            {!filterOff && (
              <Filter
                setSearchParams={setSearchParams}
                filter={filter}
                searchBox={false}
              ></Filter>
            )}
          </div>
          <>
            {bookmarks && (
              <button
                className="detele-all"
                onClick={deleteBookmarks}
                data-testid="button-bookmarks"
              >
                Delete all bookmarks
              </button>
            )}
            {hiddens && (
              <button
                className="detele-all"
                onClick={deleteHiddens}
                data-testid="button-hiddens"
              >
                Delete all hiddens
              </button>
            )}
            {modalState && (
              <Modal
                setModalState={setModalState}
                title={'Do you confirm that you want to delete all?'}
                type={typeItem}
              ></Modal>
            )}
          </>
        </div>
      </div>
    </div>
  );
};

export default FilterBar;
