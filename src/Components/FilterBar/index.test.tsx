/* eslint-disable testing-library/no-render-in-setup */
import '@testing-library/jest-dom/extend-expect';
import { act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import FilterBar from '../FilterBar';

test('of rendering of FilterBar component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <FilterBar />
    </Provider>
  );
});

const setTypeItemMocked = jest.fn();
const setModalStateMocked = jest.fn();

describe('Modal Component', () => {
  beforeEach(() => {
    renderWithWrapper(<FilterBar bookmarks hiddens filterOff></FilterBar>);
  });

  test('Call the setModalState function when user delete some item', async () => {
    const user = userEvent.setup();
    const button = screen.getByTestId('button-bookmarks');

    await act(async () => {
      await user.click(button);
    });
    expect(setModalStateMocked).toHaveBeenCalled();
    expect(setTypeItemMocked).toHaveBeenCalled();
  });

  test('Call the deleteItem function when user delete some item', async () => {
    const user = userEvent.setup();
    const button = screen.getByTestId('button-hiddens');

    await act(async () => {
      await user.click(button);
    });
    expect(setModalStateMocked).toHaveBeenCalled();
    expect(setTypeItemMocked).toHaveBeenCalled();
  });
});
