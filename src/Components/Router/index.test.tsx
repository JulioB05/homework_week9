/* eslint-disable @typescript-eslint/no-unused-vars */
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import Router from '../Router';

test('full app rendering/navigating', async () => {
  render(
    <Provider store={store}>
      <Router />
    </Provider>
  );

  expect(screen.getByText(/Some characters/i)).toBeInTheDocument();
  expect(
    screen.getByText(/With great power comes great responsibility/i)
  ).toBeInTheDocument();
});
