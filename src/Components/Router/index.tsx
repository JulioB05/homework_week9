import React from 'react';
import { BrowserRouter, Route, Routes, useLocation } from 'react-router-dom';
import Bookmarks from '../../Pages/Bookmarks';
import Characters from '../../Pages/Characters';
import Comics from '../../Pages/Comics';
import Details from '../../Pages/Details';
import Hiddens from '../../Pages/Hiddens';
import Home from '../../Pages/Home';
import Stories from '../../Pages/Stories';
import PathRoutes from '../../Utils/enumPath';

interface RouterProps {}

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid="location-display">{location.pathname}</div>;
};

const Router: React.FunctionComponent<RouterProps> = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={PathRoutes.Home} element={<Home />} />
        <Route path={PathRoutes.Characters} element={<Characters />} />
        <Route path={PathRoutes.Comics} element={<Comics />} />
        <Route path={PathRoutes.Stories} element={<Stories />} />
        <Route path={PathRoutes.Bookmarks} element={<Bookmarks />} />
        <Route path={PathRoutes.Hidden} element={<Hiddens />} />

        <Route path={PathRoutes.Details}>
          <Route index element={<Details />}></Route>
          <Route path=":type/:id" element={<Details />}></Route>
        </Route>
      </Routes>
      <LocationDisplay />
    </BrowserRouter>
  );
};

export default Router;
