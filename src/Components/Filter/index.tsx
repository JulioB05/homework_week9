import { ChangeEvent } from 'react';

import './style.scss';

interface FilterProps {
  filter?: { type: string; data: { id: number | string; name: string }[] };
  setSearchParams?: Function;
  searchBox: boolean;
}

const Filter = (props: FilterProps) => {
  const { filter, setSearchParams } = props;

  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    const filterObj = { type: filter?.type, id: e.target.value };
    setSearchParams!(filterObj);
  };

  return (
    <div className="filters">
      <label className="label"> {filter?.type.toUpperCase()} </label>
      <select
        onChange={(e) => handleChange(e)}
        className="selects"
        id={`filter-${filter?.type}`}
        data-testid="select-filter"
        placeholder="Select"
      >
        <option value={'select'}>Select a {filter?.type}</option>
        {filter?.data.map((filter) => {
          return (
            <option key={filter.id} value={filter.id}>
              {filter.name}
            </option>
          );
        })}
      </select>
    </div>
  );
};

export default Filter;
