/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable testing-library/no-render-in-setup */
import '@testing-library/jest-dom/extend-expect';
import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import renderWithWrapper from '../../testUtils/routerUtils';
import Filter from '../Filter';

test('of rendering of Filter component', () => {
  render(<Filter searchBox={false}></Filter>);

  const linkElement = screen.getByTestId('select-filter');
  expect(linkElement).toBeInTheDocument();
});

const mockFilter = {
  type: 'character',
  data: [{ id: 1009163, name: 'Aurora' }],
};
const setSearchParamsMocked = jest.fn();

describe('Filter Select', () => {
  beforeEach(() => {
    renderWithWrapper(
      <Filter
        filter={mockFilter}
        setSearchParams={setSearchParamsMocked}
        searchBox={false}
      ></Filter>
    );
  });

  test('Render a select element', () => {
    const selectItem = screen.getByTestId('select-filter');
    expect(selectItem).toBeInTheDocument();
  });

  test('Call the setSearchParams function when user typed some word', async () => {
    const user = userEvent.setup();

    await act(async () => {
      await user.selectOptions(
        screen.getByRole('combobox'),
        screen.getByRole('option', { name: 'Aurora' })
      );
    });
    expect(setSearchParamsMocked).toHaveBeenCalled();
  });
});
