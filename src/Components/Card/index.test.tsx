/* eslint-disable testing-library/prefer-screen-queries */
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Card from '../Card';

test('testing of rendering of Card', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Card id={0} title={''} imgUrl={''} body={''} date={''} category={''} />
    </Provider>
  );
});
