/* eslint-disable no-restricted-globals */
import { faEyeSlash, faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { addBookmark } from '../../Redux/Actions/bookmarkActions';
import { addHidden } from '../../Redux/Actions/hiddenActions';
import bookmarkObj from '../../Redux/Reducers/bookmarkReducer';
import PathRoutes from '../../Utils/enumPath';
import Modal from '../Modal';
import './style.scss';

interface CardProps {
  id: number;
  title: string;
  imgUrl: string;
  body: string;
  date: string;
  category: string;
  bookmarksBtn?: boolean;
  hiddensBtn?: Boolean;
}
interface bookmarkObj {
  id: number;
  name: string;
  type: string;
  img: string;
}
interface stateData {
  bookmarks: { bookmarks: bookmarkObj[] };
  hiddens: { hiddens: bookmarkObj[] };
}

const Card = (props: CardProps) => {
  const { id, title, imgUrl, body, category, bookmarksBtn, hiddensBtn } = props;
  const dispatch = useDispatch();

  const state = useSelector((state) => state);
  const typedState = state as stateData;
  const { bookmarks, hiddens } = typedState;

  const [modalState, setModalState] = useState(false);
  const [typeItem, setTypeItem] = useState('');

  const functionClickBookmark = () => {
    const data = { id: id, name: title, type: category, img: imgUrl };
    const existData = bookmarks?.bookmarks.filter((el) => el.id === id);
    document
      .getElementById(`${category}/${id}/bookmark`)
      ?.classList.add('heart-active');

    if (existData.length < 1) {
      dispatch(addBookmark(data));
    }
  };

  const functionClickHidden = () => {
    const data = { id: id, name: title, type: category, img: imgUrl };
    const existData = hiddens?.hiddens.filter((el) => el.id === id);
    document
      .getElementById(`${category}/${id}/hide`)
      ?.classList.add('hide-active');

    if (existData.length < 1) {
      dispatch(addHidden(data));
    }
  };

  const deleteBookmark = () => {
    setTypeItem('bookmark');
    setModalState(true);
  };
  const deleteHidden = () => {
    setTypeItem('hidden');
    setModalState(true);
  };

  return (
    <div className="card">
      <Link to={`${PathRoutes.Details}/${category}/${id}`} className="link-det">
        <div className="container">
          <img
            className="imageNew"
            src={imgUrl ? imgUrl : 'https://i.gifer.com/3sqI.gif'}
            alt={title}
          ></img>
        </div>

        <div className="details">
          <h3>{title}</h3>
          <p> {body} </p>
          <p> Type: {category}</p>
        </div>
      </Link>

      <div className="icon-bookmark">
        <FontAwesomeIcon
          icon={faEyeSlash}
          onClick={functionClickHidden}
          className="hide"
          id={`${category}/${id}/hide`}
        />

        <FontAwesomeIcon
          icon={faHeart}
          onClick={functionClickBookmark}
          className={`heart`}
          id={`${category}/${id}/bookmark`}
        />
      </div>

      {bookmarksBtn && (
        <button onClick={deleteBookmark} className="button-delete">
          Delete bookmark
        </button>
      )}

      {hiddensBtn && (
        <button onClick={deleteHidden} className="button-delete">
          Delete item
        </button>
      )}

      {modalState && (
        <Modal
          setModalState={setModalState}
          title={title}
          id={id}
          type={typeItem}
          category={category}
        ></Modal>
      )}
    </div>
  );
};

export default Card;
