import '@testing-library/jest-dom/extend-expect';
import { screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import DetailCard from '../DetailCard';
import dataDetailsMocked from './dataDetailsMocked';

describe('testing of rendering of DetailCard', () => {
  renderWithWrapper(
    <Provider store={store}>
      <DetailCard data={dataDetailsMocked} />
    </Provider>
  );

  test('find characters', () => {
    const character = screen.getByText('A-Bomb (HAS');
    expect(character).toBeInTheDocument();
  });
});
