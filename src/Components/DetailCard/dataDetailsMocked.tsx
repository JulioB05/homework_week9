import { Comics } from '../../Interfaces/apiCharacters';
import { Characters, ItemType, Stories } from '../../Interfaces/apiComics';

interface DetailsCardProps {
  id: number;
  title: string;
  body: string;
  image: string;
  comics?: Comics | null;
  characters?: Characters | null;
  stories?: Stories | null;
}

const dataDetailsMocked: DetailsCardProps = {
  id: 1017100,
  title: 'A-Bomb (HAS)',
  body: "Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! ",
  image: 'http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16.jpg',
  comics: {
    available: 4,
    collectionURI:
      'http://gateway.marvel.com/v1/public/characters/1017100/comics',
    items: [
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/47176',
        name: 'FREE COMIC BOOK DAY 2013 1 (2013) #1',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/40632',
        name: 'Hulk (2008) #53',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/40630',
        name: 'Hulk (2008) #54',
      },
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/40628',
        name: 'Hulk (2008) #55',
      },
    ],
    returned: 4,
  },
  stories: {
    available: 7,
    collectionURI:
      'http://gateway.marvel.com/v1/public/characters/1017100/stories',
    items: [
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/92078',
        name: 'Hulk (2008) #55',
        type: ItemType.Cover,
      },

      {
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/92082',
        name: 'Hulk (2008) #54',
        type: ItemType.Cover,
      },

      {
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/92086',
        name: 'Hulk (2008) #53',
        type: ItemType.Cover,
      },

      {
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/105929',
        name: 'cover from Free Comic Book Day 2013 (Avengers/Hulk) (2013) #1',
        type: ItemType.Cover,
      },
    ],
    returned: 7,
  },
  characters: {
    available: 1,
    collectionURI: 'http://gateway.marvel.com/v1/public/comics/1308/characters',
    items: [
      {
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009610',
        name: 'Spider-Man (Peter Parker)',
      },
    ],
    returned: 1,
  },
};

export default dataDetailsMocked;
