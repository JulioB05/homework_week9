/* eslint-disable testing-library/no-render-in-setup */

import '@testing-library/jest-dom/extend-expect';
import { act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Modal from '../Modal';

const setModalStateMocked = jest.fn;

describe('Modal Component', () => {
  beforeEach(() => {
    renderWithWrapper(
      <Provider store={store}>
        <Modal
          type={'bookmarks'}
          setModalState={setModalStateMocked}
          title={'test'}
          id={2}
        ></Modal>
      </Provider>
    );
  });

  test('Call the setModalState function when user access to Modal', async () => {
    const user = userEvent.setup();
    const button = screen.getByTestId('button-close');

    await act(async () => {
      await user.click(button);
    });
    expect(setModalStateMocked).toHaveBeenCalled();
  });

  test('Call the deleteItem function when user delete some item', async () => {
    const user = userEvent.setup();
    const button = screen.getByTestId('button-delete');

    await act(async () => {
      await user.click(button);
    });
    expect(setModalStateMocked).toHaveBeenCalled();
  });
});
