import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Dispatch, SetStateAction } from 'react';
import { useDispatch } from 'react-redux';
import {
  deleteAllBookmark,
  deleteOneBookmark,
} from '../../Redux/Actions/bookmarkActions';
import {
  deleteAllHidden,
  deleteOneHidden,
} from '../../Redux/Actions/hiddenActions';
import './style.scss';

interface modalProps {
  setModalState?: Dispatch<SetStateAction<boolean>>;
  title?: string;
  id?: number;
  type: string;
  category?: string;
}
const Modal = (props: modalProps) => {
  const { setModalState, title, id, type, category } = props;
  const dispatch = useDispatch();

  const deleteItem = (type: string) => {
    if (type === 'bookmark') {
      dispatch(deleteOneBookmark(id!));
      setModalState!(false);
    } else if (type === 'hidden') {
      dispatch(deleteOneHidden(id!));
      setModalState!(false);
    } else if (type === 'all-bookmarks') {
      dispatch(deleteAllBookmark());
      setModalState!(false);
    } else if (type === 'all-hiddens') {
      dispatch(deleteAllHidden());
      setModalState!(false);
    }
  };

  return (
    <>
      <div className="overlay">
        <div className="container-modal">
          <div className="title-cotainer">
            <h3 className="title">Are you sure you want to delete?</h3>
          </div>

          <button
            data-testid="button-close"
            className="btn-close"
            onClick={() => setModalState!(false)}
          >
            <FontAwesomeIcon icon={faXmark} />
          </button>

          <div className="container-data">
            <h1>{title}</h1>

            {id ? (
              <>
                <p>ID: {id}</p>
                <p>Category: {category}</p>
              </>
            ) : (
              ''
            )}

            <button
              data-testid="button-delete"
              className="button-delete"
              onClick={() => deleteItem(type)}
            >
              Confirm
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default Modal;
