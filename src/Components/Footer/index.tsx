import React, { Dispatch, SetStateAction } from 'react';
import Paginator from '../Paginator';
import './style.scss';

interface FooterProps {
  currentPage: number;
  setCurrentPage: Dispatch<SetStateAction<number>>;
  setSearchParams: Function;
  sizeOfData: number;
  params?: { page: string | null; name: string | null };
}

const Footer: React.FunctionComponent<FooterProps> = ({
  currentPage,
  setCurrentPage,
  setSearchParams,
  sizeOfData,
  params,
}) => {
  return (
    <div>
      <Paginator
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        setSearchParams={setSearchParams}
        sizeOfData={sizeOfData}
        params={params}
      ></Paginator>
    </div>
  );
};

export default Footer;
