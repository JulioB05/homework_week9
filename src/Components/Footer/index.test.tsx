import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Footer from '../Footer';

test('of rendering of Footer component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Footer
        currentPage={0}
        sizeOfData={0}
        setCurrentPage={function (value: React.SetStateAction<number>): void {
          throw new Error('Function not implemented.');
        }}
        setSearchParams={Function}
      />
    </Provider>
  );
});
