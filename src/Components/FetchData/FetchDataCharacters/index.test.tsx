import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../../Redux/Store';
import renderWithWrapper from '../../../testUtils/routerUtils';
import FetchDataCharacters from '../FetchDataCharacters';
import mockData from './mockData';

test('testing of rendering of FetchDataCharacters', () => {
  renderWithWrapper(
    <Provider store={store}>
      <FetchDataCharacters
        params={{
          page: undefined,
          name: undefined,
        }}
        apiStatus={false}
        data={mockData}
      />
    </Provider>
  );
});
