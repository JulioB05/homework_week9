import { useEffect, useMemo, useState } from 'react';
import useFiltered from '../../../Hooks/useFiltered';
import { Data } from '../../../Interfaces/apiCharacters';
import Card from '../../Card';
import Loader from '../../Loader';
import './style.scss';

interface FetchDataCharactersProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
  data: Data;
}

const FetchDataCharacters = (props: FetchDataCharactersProps) => {
  const { params, apiStatus, data } = props;
  const { hiddenIds, favoritesIds, numberHiddens, numberBookmarks } =
    useFiltered('characters');
  const [filteredIds, setFilteredIds] = useState([0]);

  useEffect(() => {
    const ids = hiddenIds();
    setFilteredIds(ids);
  }, [numberHiddens]);

  useEffect(() => {
    favoritesIds();
  }, [params]);

  const filteredCharacters = useMemo(() => {
    return data?.results.filter(
      (character) => !filteredIds.includes(character.id)
    );
  }, [numberHiddens, numberBookmarks, data, params.page]);

  return (
    <div className="body-fetch-data-characters">
      <h1 className="title-products">Characters</h1>
      <div className="cards">
        {data ? (
          filteredCharacters.map(
            (character: {
              modified: string;
              description: string;
              thumbnail: any;
              name: string;
              id: number;
            }) => {
              let img = `${character.thumbnail.path}.${character.thumbnail.extension}`;
              return (
                <Card
                  key={character.id}
                  id={character.id}
                  title={character.name}
                  imgUrl={img}
                  body={character.description}
                  date={''}
                  category={'characters'}
                ></Card>
              );
            }
          )
        ) : (
          <>
            {apiStatus ? (
              <Loader apiState={apiStatus} />
            ) : (
              <p className="not-found">Not found: {params.name}</p>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default FetchDataCharacters;
