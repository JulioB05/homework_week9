import { Data, Modified, Type } from '../../../Interfaces/apiStories';

const mockData: Data = {
  offset: 0,
  limit: 20,
  total: 121929,
  count: 20,
  results: [
    {
      id: 7,
      title:
        'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf',
      description: '',
      resourceURI: 'http://gateway.marvel.com/v1/public/stories/7',
      type: Type.Story,
      modified: Modified.The19691231T1900000500,
      thumbnail: null,
      creators: {
        available: 0,
        collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/creators',
        items: [],
        returned: 0,
      },
      characters: {
        available: 0,
        collectionURI:
          'http://gateway.marvel.com/v1/public/stories/7/characters',
        items: [],
        returned: 0,
      },
      series: {
        available: 1,
        collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/series',
        items: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/series/6',
            name: 'CAGE HC (2002)',
          },
        ],
        returned: 1,
      },
      comics: {
        available: 1,
        collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/comics',
        items: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/941',
            name: 'CAGE HC (Hardcover)',
          },
        ],
        returned: 1,
      },
      events: {
        available: 0,
        collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/events',
        items: [],
        returned: 0,
      },
      originalIssue: {
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/941',
        name: 'CAGE HC (Hardcover)',
      },
    },
  ],
};

export default mockData;
