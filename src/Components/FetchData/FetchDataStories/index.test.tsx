import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../../Redux/Store';
import renderWithWrapper from '../../../testUtils/routerUtils';
import FetchDataStories from '../FetchDataStories';
import mockData from '../FetchDataStories/mockData';

test('testing of rendering of FetchDaraStories', () => {
  renderWithWrapper(
    <Provider store={store}>
      <FetchDataStories
        params={{
          page: undefined,
          name: undefined,
        }}
        apiStatus={false}
        data={mockData}
      />
    </Provider>
  );
});
