import { useEffect, useMemo, useState } from 'react';
import useFiltered from '../../../Hooks/useFiltered';
import { Data } from '../../../Interfaces/apiStories';
import Card from '../../Card';
import Loader from '../../Loader';
import './style.scss';

interface FetchDataStoriesProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
  data: Data;
}

const FetchDataStories = (props: FetchDataStoriesProps) => {
  const { apiStatus, data, params } = props;
  const { hiddenIds, favoritesIds, numberHiddens, numberBookmarks } =
    useFiltered('stories');
  const [filteredIds, setFilteredIds] = useState([0]);

  useEffect(() => {
    const ids = hiddenIds();
    setFilteredIds(ids);
  }, [numberHiddens]);

  useEffect(() => {
    favoritesIds();
  }, [params]);

  const filteredCharacters = useMemo(() => {
    return data?.results.filter(
      (character) => !filteredIds.includes(character.id)
    );
  }, [numberHiddens, numberBookmarks, data, params.page]);

  return (
    <div className="body-fetch-data-stories">
      <h1 className="title-products">Stories</h1>
      <div className="cards-stories">
        {data ? (
          filteredCharacters.map(
            (story: {
              description: string;
              modified: string;
              id: number;
              title: string;
            }) => {
              let img = `https://www.universomarvel.com/wp-content/uploads/2015/04/marvel_studios_background_by_diamonddesignhd-d5n6pg3.png`;
              return (
                <Card
                  key={story.id}
                  id={story.id}
                  title={''}
                  imgUrl={img}
                  body={story.title}
                  date={''}
                  category={'stories'}
                ></Card>
              );
            }
          )
        ) : (
          <Loader apiState={apiStatus}></Loader>
        )}
      </div>
    </div>
  );
};

export default FetchDataStories;
