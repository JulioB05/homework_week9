import { useEffect, useMemo, useState } from 'react';
import useFiltered from '../../../Hooks/useFiltered';
import { Data } from '../../../Interfaces/apiComics';
import Card from '../../Card';
import Loader from '../../Loader';
import './style.scss';

interface FetchDataComicsProps {
  params: { page?: string | null; name?: string | null };
  apiStatus: boolean;
  data: Data;
}

const FetchDataComics = (props: FetchDataComicsProps) => {
  const { params, apiStatus, data } = props;
  const { hiddenIds, favoritesIds, numberHiddens, numberBookmarks } =
    useFiltered('comics');
  const [filteredIds, setFilteredIds] = useState([0]);

  useEffect(() => {
    const ids = hiddenIds();
    setFilteredIds(ids);
  }, [numberHiddens]);

  useEffect(() => {
    favoritesIds();
  }, [params]);

  const filteredComics = useMemo(() => {
    return data?.results.filter((comic) => !filteredIds.includes(comic.id));
  }, [numberHiddens, numberBookmarks, data, params.page]);

  return (
    <div className="body-fetch-data-comics">
      <h1 className="title-products">Comics</h1>
      <div className="cards-comics">
        {data ? (
          filteredComics.map(
            (comic: {
              modified: string;
              format: string;
              id: number;
              thumbnail: any;
              title: string;
            }) => {
              let img = `${comic.thumbnail.path}.${comic.thumbnail.extension}`;
              return (
                <Card
                  key={comic.id}
                  id={comic.id}
                  title={comic.title}
                  imgUrl={img}
                  body={comic.format}
                  date={comic.modified}
                  category={'comics'}
                ></Card>
              );
            }
          )
        ) : (
          <>
            {apiStatus ? (
              <Loader apiState={apiStatus} />
            ) : (
              <p className="not-found">Not found: {params.name}</p>
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default FetchDataComics;
