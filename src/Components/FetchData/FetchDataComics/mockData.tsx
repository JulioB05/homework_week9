import {
  Data,
  DateType,
  DiamondCode,
  Extension,
  Format,
  ItemType,
  PriceType,
  Role,
  URLType,
} from '../../../Interfaces/apiComics';

const mockData: Data = {
  offset: 0,
  limit: 20,
  total: 52839,
  count: 20,
  results: [
    {
      id: 82967,
      digitalId: 0,
      title: 'Marvel Previews (2017)',
      issueNumber: 0,
      variantDescription: '',
      description: null,
      modified: '2019-11-07T08:46:15-0500',
      isbn: '',
      upc: '75960608839302811',
      diamondCode: DiamondCode.Empty,
      ean: '',
      issn: '',
      format: Format.Empty,
      pageCount: 112,
      textObjects: [],
      resourceURI: 'http://gateway.marvel.com/v1/public/comics/82967',
      urls: [
        {
          type: URLType.Detail,
          url: 'http://marvel.com/comics/issue/82967/marvel_previews_2017?utm_campaign=apiRef&utm_source=f5bf1060852e4bb6f0712efc5b34341b',
        },
      ],
      series: {
        resourceURI: 'http://gateway.marvel.com/v1/public/series/23665',
        name: 'Marvel Previews (2017 - Present)',
      },
      variants: [
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/82965',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/82970',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/82969',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/74697',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/72736',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/75668',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/65364',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/65158',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/65028',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/75662',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/74320',
          name: 'Marvel Previews (2017)',
        },
        {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/73776',
          name: 'Marvel Previews (2017)',
        },
      ],
      collections: [],
      collectedIssues: [],
      dates: [
        {
          type: DateType.FocDate,
          date: '2099-10-30T00:00:00-0500',
        },
        {
          type: DateType.FocDate,
          date: '2019-10-07T00:00:00-0400',
        },
      ],
      prices: [
        {
          type: PriceType.PrintPrice,
          price: 0,
        },
      ],
      thumbnail: {
        path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
        extension: Extension.Jpg,
      },
      images: [],
      creators: {
        available: 1,
        collectionURI:
          'http://gateway.marvel.com/v1/public/comics/82967/creators',
        items: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/creators/10021',
            name: 'Jim Nausedas',
            role: Role.Editor,
          },
        ],
        returned: 1,
      },
      characters: {
        available: 0,
        collectionURI:
          'http://gateway.marvel.com/v1/public/comics/82967/characters',
        items: [],
        returned: 0,
      },
      stories: {
        available: 2,
        collectionURI:
          'http://gateway.marvel.com/v1/public/comics/82967/stories',
        items: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/stories/183698',
            name: 'cover from Marvel Previews (2017)',
            type: ItemType.Cover,
          },
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/stories/183699',
            name: 'story from Marvel Previews (2017)',
            type: ItemType.Cover,
          },
        ],
        returned: 2,
      },
      events: {
        available: 0,
        collectionURI:
          'http://gateway.marvel.com/v1/public/comics/82967/events',
        items: [],
        returned: 0,
      },
    },
  ],
};

export default mockData;
