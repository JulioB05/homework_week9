import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../../Redux/Store';
import renderWithWrapper from '../../../testUtils/routerUtils';
import FetchDataComics from '../FetchDataComics';
import mockData from '../FetchDataComics/mockData';

test('testing of rendering of FetchDataComics', () => {
  renderWithWrapper(
    <Provider store={store}>
      <FetchDataComics
        params={{
          page: undefined,
          name: undefined,
        }}
        apiStatus={false}
        data={mockData}
      />
    </Provider>
  );
});
