/* eslint-disable testing-library/no-render-in-setup */
import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../Redux/Store';
import renderWithWrapper from '../../testUtils/routerUtils';
import Header from '../Header';

test('of rendering of Header component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <Header />
    </Provider>
  );
});

// const handleClickMocked = jest.fn();

// describe('Header Component', () => {
//   beforeEach(() => {
//     renderWithWrapper(<Header></Header>);
//   });

//   test('Call the setClicked function when user click on the burguer button', async () => {
//     const user = userEvent.setup();
//     const button = screen.getByTestId('button-burguer');

//     await act(async () => {
//       await user.click(button);
//     });
//     expect(handleClickMocked).toHaveBeenCalled();
//   });
// });
