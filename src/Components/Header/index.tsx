/* eslint-disable no-restricted-globals */

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import hydra from '../../Images/hydra.png';
import marvelLogo from '../../Images/marvel-logo.png';
import shield from '../../Images/shield.png';
import spider from '../../Images/spider.png';
import wanda from '../../Images/wanda.png';
import PathRoutes from '../../Utils/enumPath';
import BurguerButton from './BurguerButton';
import './style.scss';

interface HeaderProps {}

const Header: React.FunctionComponent<HeaderProps> = () => {
  const [clicked, setClicked] = useState(false);

  return (
    <div>
      <header>
        <div className="navContainer">
          <Link to={PathRoutes.Home} className="logo-link">
            <img src={marvelLogo} alt="" className="logo" />
          </Link>
          <div className="container-logos">
            <img src={shield} alt="shield" className="shield" />
            <img src={hydra} alt="shield" className="hydra" />
            <img src={wanda} alt="shield" className="wanda" />
            <img src={spider} alt="shield" className="spider" />
          </div>

          <div className={`links ${clicked ? 'active' : ''}`}>
            <Link to={PathRoutes.Home} className="link">
              Home
            </Link>

            <Link to={PathRoutes.Characters} className="link">
              Characters
            </Link>
            <Link to={PathRoutes.Comics} className="link">
              Comics
            </Link>
            <Link to={PathRoutes.Stories} className="link">
              Stories
            </Link>
            <Link to={PathRoutes.Bookmarks} className="link">
              Bookmarks
            </Link>
            <Link to={PathRoutes.Hidden} className="link">
              Hiddens
            </Link>
          </div>

          <div className="burguer">
            <BurguerButton
              handleClick={() => {
                setClicked(!clicked);
              }}
              clicked={clicked}
            ></BurguerButton>
          </div>

          <div className={`BgDiv initial ${clicked ? ' active' : ''}`}></div>
        </div>
      </header>
    </div>
  );
};

export default Header;
