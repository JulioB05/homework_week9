import '@testing-library/jest-dom/extend-expect';
import { Provider } from 'react-redux';
import { store } from '../../../Redux/Store';
import renderWithWrapper from '../../../testUtils/routerUtils';
import BurguerButton from '../BurguerButton';

test('of rendering of BurguerButton component', () => {
  renderWithWrapper(
    <Provider store={store}>
      <BurguerButton
        clicked={false}
        handleClick={function (): void {
          throw new Error('Function not implemented.');
        }}
      />
    </Provider>
  );
});
